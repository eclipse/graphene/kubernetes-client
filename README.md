
# Graphene Kubernetes Client

This repository holds components and tools supporting deployment of Graphene
ai-models ("solutions") under kubernetes. 

Please see the documentation in the "docs" folder.
